package creator.abstractfactory;

public interface Receiver {
	String receive();
}
