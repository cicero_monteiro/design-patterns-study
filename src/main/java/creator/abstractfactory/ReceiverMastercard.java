package creator.abstractfactory;

public class ReceiverMastercard implements Receiver {

	public String receive() {
		System.out.println("Received message Mastercard.");
		return "Mastercard message";
	}

}
