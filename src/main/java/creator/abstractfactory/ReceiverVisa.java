package creator.abstractfactory;

public class ReceiverVisa implements Receiver {

	public String receive() {
		System.out.println("Received message visa.");
		return "Visa message";
	}

}
