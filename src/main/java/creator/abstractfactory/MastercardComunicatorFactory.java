package creator.abstractfactory;

public class MastercardComunicatorFactory implements ComunicatorFactory {
	
	private SenderCreator senderCreator = new SenderCreator();
	private ReceiverCreator receiverCreator = new ReceiverCreator();
	

	public Sender createSender() {
		return this.senderCreator.create(SenderCreator.MASTERCARD);
	}

	public Receiver createReceiver() {
		return this.receiverCreator.create(receiverCreator.MASTERCARD);
	}

}
