package creator.abstractfactory;

public class SenderMastercard implements Sender {

	public void send(String message) {
		System.out.println("Sending the message to the Mastercard");
		System.out.println(message);
	}
	
}
