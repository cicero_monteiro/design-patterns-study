package creator.abstractfactory;

public interface ComunicatorFactory {
	Sender createSender();
	Receiver createReceiver();
}
