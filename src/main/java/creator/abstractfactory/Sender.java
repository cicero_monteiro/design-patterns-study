package creator.abstractfactory;

public interface Sender {
	void send(String message);
}
