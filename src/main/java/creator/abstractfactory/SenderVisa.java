package creator.abstractfactory;

public class SenderVisa implements Sender {

	public void send(String message) {
		System.out.println("Sending the message to the Visa");
		System.out.println(message);
	}
	
}
