package creator.abstractfactory;

public class ReceiverCreator {
	
	public static final int VISA = 0;
	public static final int MASTERCARD = 1;
	
	public Receiver create(int receiverType) {
		if (SenderCreator.VISA == receiverType) {
			return new ReceiverVisa();
		} else if (SenderCreator.MASTERCARD == receiverType) {
			return new ReceiverMastercard();
		} else {
			throw new IllegalArgumentException("Receiver type not suported.");
		}
	}
	

}
