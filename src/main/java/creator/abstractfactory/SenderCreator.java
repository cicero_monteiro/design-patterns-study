package creator.abstractfactory;

public class SenderCreator {
	
	public static final int VISA = 0;
	public static final int MASTERCARD = 1;
	
	public Sender create(int senderType) {
		if (SenderCreator.VISA == senderType) {
			return new SenderVisa();
		} else if (SenderCreator.MASTERCARD == senderType) {
			return new SenderMastercard();
		} else {
			throw new IllegalArgumentException("Sender type not suported.");
		}
	}
	

}
