package creator.abstractfactory;

public class VisaComunicatorFactory implements ComunicatorFactory {
	
	private SenderCreator senderCreator = new SenderCreator();
	private ReceiverCreator receiverCreator = new ReceiverCreator();
	

	public Sender createSender() {
		return this.senderCreator.create(SenderCreator.VISA);
	}

	public Receiver createReceiver() {
		return this.receiverCreator.create(receiverCreator.VISA);
	}

}
