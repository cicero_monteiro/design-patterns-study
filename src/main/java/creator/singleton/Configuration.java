package creator.singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Configuration {
	
	private Map<String, String> properties;
	private static Configuration instance;
	
	private Configuration() {
		this.properties = new HashMap<>();
		this.properties.put("time-zone", "America/Sao_Paulo");
		this.properties.put("currency-code", "BRL");
	}
	
	public static Configuration getInstance() {
		
		if (Configuration.instance == null) {
			Configuration.instance = new Configuration();
		}
		return Configuration.instance;
	}
	
	public String getProperty(String propertyName) {
		return this.properties.get(propertyName);
	}

}
