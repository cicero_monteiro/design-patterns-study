package creator.factorymethod;

public interface Sender {
	void send(String message);
}
