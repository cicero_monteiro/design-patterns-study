package creator.factorymethod;

public class SenderCreator {
	
	public Sender create(SenderType senderType) {
		switch (senderType) {
		case SMS:
			return new SenderSMS();
		case EMAIL:
			return new SenderEmail();
		case JMS:
			return new SenderJMS();

		default:
			throw new IllegalArgumentException("Sender type not supported");
		}
	}

}
