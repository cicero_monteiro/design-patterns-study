package creator.factorymethod;

public class SenderSMS implements Sender {

	public void send(String message) {
		System.out.println("Sending by SMS a message: " + message);
	}

}
