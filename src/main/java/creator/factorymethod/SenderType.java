package creator.factorymethod;

public enum SenderType {
	
	SMS(0), EMAIL(1), JMS(2);
	
	private int code;
	
	private SenderType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
