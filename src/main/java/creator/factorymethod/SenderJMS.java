package creator.factorymethod;

public class SenderJMS implements Sender {

	public void send(String message) {
		System.out.println("Sending by JMS a message: " + message);
	}

}
