package creator.factorymethod;

public class SenderEmail implements Sender {

	public void send(String message) {
		System.out.println("Sending by Email a message: " + message);
	}

}
