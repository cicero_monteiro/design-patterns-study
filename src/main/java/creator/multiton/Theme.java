package creator.multiton;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

public class Theme {

	private String name;
	private Color background;
	private Color font;
	private static Map<String, Theme> themes = new HashMap<>();
	public static final String SKY = "Sky";
	public static final String FIRE = "Fire";
	
	private Theme() {
		
	}

	static {
		Theme theme1 = new Theme();
		theme1.setName(Theme.SKY);
		theme1.setBackground(Color.BLUE);
		theme1.setFont(Color.BLACK);
		
		Theme theme2 = new Theme();
		theme2.setName(Theme.FIRE);
		theme2.setBackground(Color.RED);
		theme2.setFont(Color.WHITE);
		
		themes.put(theme1.getName(), theme1);
		themes.put(theme2.getName(), theme2);

	}
	
	public static Theme getInstance(String themeName) {
		return Theme.themes.get(themeName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getBackground() {
		return background;
	}

	public void setBackground(Color background) {
		this.background = background;
	}

	public Color getFont() {
		return font;
	}

	public void setFont(Color font) {
		this.font = font;
	}

}
