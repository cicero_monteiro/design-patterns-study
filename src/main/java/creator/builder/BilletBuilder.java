package creator.builder;
import java.util.Calendar;

public interface BilletBuilder {
	void buildPayer(String payer);
	void buildAssignor(String assignor);
	void buildValue(double value);
	void buildPay(Calendar pay);
	void buildOurNumber(int ourNumber);
	
	Billet getBillet();
}
