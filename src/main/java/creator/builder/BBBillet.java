package creator.builder;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BBBillet implements Billet {
	
	private String payer;
	private String assignor;
	private double value;
	private Calendar pay;
	private int ourNumber;

	public BBBillet(String payer, String assignor, double value, Calendar pay, int ourNumber) {
		super();
		this.payer = payer;
		this.assignor = assignor;
		this.value = value;
		this.pay = pay;
		this.ourNumber = ourNumber;
	}

	public String getPayer() {
		return this.payer;
	}

	public String getAssignor() {
		return this.assignor;
	}

	public double getValue() {
		return this.value;
	}

	public Calendar getPay() {
		return this.pay;
	}

	public int getOurNumber() {
		return this.ourNumber;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Billet BB");
		sb.append("\n");
		
		sb.append("Payer: " + this.getPayer());
		sb.append("\n");
		
		sb.append("Assignor: " + this.assignor);
		sb.append("\n");
		
		sb.append("Value: " + this.value);
		sb.append("\n");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String format = sdf.format(getPay().getTime());
		
		sb.append("Pay: " + format);
		sb.append("\n");
		
		sb.append("OurNumber: " + this.getOurNumber());
		sb.append("\n");
		
		return sb.toString();
	}

}
