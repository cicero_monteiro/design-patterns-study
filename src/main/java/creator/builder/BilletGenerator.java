package creator.builder;

import java.util.Calendar;

public class BilletGenerator {
	
	private BilletBuilder billetBuilder;
	
	public BilletGenerator(BilletBuilder billetBuilder) {
		this.billetBuilder = billetBuilder;
	}
	
	public Billet generateBillet() {
		this.billetBuilder.buildPayer("Cicero Monteiro");
		this.billetBuilder.buildAssignor("Percy Oliveira");
		this.billetBuilder.buildValue(100.54);
		
		Calendar pay = Calendar.getInstance();
		pay.add(Calendar.DATE, 30);
		
		this.billetBuilder.buildPay(pay);
		this.billetBuilder.buildOurNumber(1234);
		
		Billet billet = billetBuilder.getBillet();
		return billet;
		
	}

}
