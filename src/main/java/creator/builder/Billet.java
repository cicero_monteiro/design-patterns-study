package creator.builder;

import java.util.Calendar;

public interface Billet {
	String getPayer();
	String getAssignor();
	double getValue();
	Calendar getPay();
	int getOurNumber();
	String toString();
}
