package creator.builder;

import java.util.Calendar;

public class BBBilletBuilder implements BilletBuilder {
	
	private String payer;
	private String assignor;
	private double value;
	private Calendar pay;
	private int ourNumber;

	public void buildPayer(String payer) {
		this.payer = payer;
	}

	public void buildAssignor(String assignor) {
		this.assignor = assignor;
	}

	public void buildValue(double value) {
		this.value = value;
	}

	public void buildPay(Calendar pay) {
		this.pay = pay;
	}

	public void buildOurNumber(int ourNumber) {
		this.ourNumber = ourNumber;
	}

	public Billet getBillet() {
		return new BBBillet(payer, assignor, value, pay, ourNumber);
	}

}
