package creator.prototype;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import com.sun.xml.internal.ws.api.server.SDDocumentFilter;

public class Campaign implements Prototype<Campaign> {

	private String name;
	private Calendar pay;
	private Set<String> keyWords;

	public Campaign(String name, Calendar pay, Set<String> keyWords) {
		super();
		this.name = name;
		this.pay = pay;
		this.keyWords = keyWords;
	}

	public String getName() {
		return name;
	}

	public Calendar getPay() {
		return pay;
	}

	public Set<String> getKeyWords() {
		return keyWords;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Campaign clone() {
		String name = "Copy of Campaign: " + this.name;
		Calendar pay = (Calendar) this.pay.clone();
		Set<String> keyWords = new HashSet(this.keyWords);
		Campaign campaign = new Campaign(name, pay, keyWords);
		
		return campaign;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("-----------------");
		sb.append("\n");
		sb.append("Campaign name: ");
		sb.append(this.name);
		sb.append("\n");	
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String format = sdf.format(this.pay.getTime());
		sb.append("Pay: " + format);
		sb.append("\n");
		
		sb.append("Keywords: \n");
		this.keyWords.forEach(s -> sb.append(" - " + s + "\n"));
		
		sb.append("-----------------");
		sb.append("\n");
		
		
		return sb.toString();
	}
}
