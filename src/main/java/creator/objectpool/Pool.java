package creator.objectpool;

public interface Pool<T> {
 T acquire();
 void release(Employee employee);
}
