package creator.objectpool;

import java.util.ArrayList;
import java.util.List;

public class EmployeePool implements Pool<Employee> {

	private List<Employee> employees;

	public EmployeePool() {
		this.employees = new ArrayList();
		this.employees.add(new Employee("Marcelo Martins"));
		this.employees.add(new Employee("Rafael Cosentino"));
		this.employees.add(new Employee("Jonas Hirata"));

	}

	@Override
	public Employee acquire() {
		if(this.employees.size() > 0) {
			return this.employees.remove(0);
		}
		return null;
	}

	@Override
	public void release(Employee employee) {
		this.employees.add(employee);
	}

}
