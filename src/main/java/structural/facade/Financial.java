package structural.facade;

public class Financial {
	
	public void invoice(String customer, String product) {
		System.out.println("Invoice: ");
		System.out.println("Customer: " + customer);
		System.out.println("Product: " + product);
	}

}
