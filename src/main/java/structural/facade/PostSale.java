package structural.facade;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PostSale {
	
	public void scheduleContact(String customer, String product) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 30);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String format = sdf.format(calendar.getTime());
		
		System.out.println("Get in touch with " + customer + "on product " + product + " in day " + format);
	}

}
