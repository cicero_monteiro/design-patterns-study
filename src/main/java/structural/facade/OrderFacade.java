package structural.facade;

public class OrderFacade {
	
	private Stock stock;
	private Financial financial;
	private PostSale postSale;
	
	public OrderFacade(Stock stock, Financial financial, PostSale postSale) {
		super();
		this.stock = stock;
		this.financial = financial;
		this.postSale = postSale;
	}
	
	public void scribeOrder(Order order) {
		this.stock.sendProduct(order.getProduct(), order.getAddress());
		this.financial.invoice(order.getCustomer(), order.getProduct());
		this.postSale.scheduleContact(order.getCustomer(), order.getProduct());
	}
	
	
	

}
