package structural.facade;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Stock {

	public void sendProduct(String product, String addressDelivery) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 2);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String format = sdf.format(calendar.getTime());

		System.out.println("The product " + product + " it will be delivered until the 18 day " + format);
	}

}
