package structural.facade;

public class Order {

	private String product;
	private String customer;
	private String address;

	public Order(String product, String customer, String address) {
		super();
		this.product = product;
		this.customer = customer;
		this.address = address;
	}

	public String getProduct() {
		return product;
	}

	public String getCustomer() {
		return customer;
	}

	public String getAddress() {
		return address;
	}

}
