package structural.bridge;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Receipt implements Document {
	
	private String sender;
	private String assignor;
	private double value;
	private GeneratorFile generatorFile;
	

	public Receipt(String sender, String assignor, double value, GeneratorFile generatorFile) {
		super();
		this.sender = sender;
		this.assignor = assignor;
		this.value = value;
		this.generatorFile = generatorFile;
	}

	@Override
	public void generateFile() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Recibo:");
		buffer.append("\n");
		buffer.append("Empresa:" + this.sender);
		buffer.append("\n");
		buffer.append("Cliente: " + this.assignor);
		buffer.append("\n");
		buffer.append("Valor: " + this.value);
		buffer.append("\n");
		this.generatorFile.generate(buffer.toString());
	}

}
