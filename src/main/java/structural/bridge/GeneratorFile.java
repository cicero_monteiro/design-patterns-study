package structural.bridge;

public interface GeneratorFile {
	void generate(String content);
}
