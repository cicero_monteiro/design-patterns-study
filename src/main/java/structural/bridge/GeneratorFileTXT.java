package structural.bridge;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class GeneratorFileTXT implements GeneratorFile {

	@Override
	public void generate(String content) {
		try(PrintStream exit = new PrintStream("file.txt")) {
			exit.println(content);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
