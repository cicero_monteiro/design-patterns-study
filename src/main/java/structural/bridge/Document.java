package structural.bridge;

public interface Document {
	void generateFile();
}
