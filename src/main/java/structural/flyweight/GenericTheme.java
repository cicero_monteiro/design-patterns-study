package structural.flyweight;

import java.util.Arrays;

public class GenericTheme implements FlyweightTheme {

    @Override
    public void print(String title, String text) {
	System.out.println("########## " + title.toUpperCase() + "##########");
	System.out.println(text);
	char[] footerE = new char[(int) Math.floor((6 + title.length()) / 2.0)];
	char[] footerD = new char[(int) Math.ceil((6 + title.length()) / 2.0)];
	Arrays.fill(footerE, '#');
	Arrays.fill(footerD, '#');
	System.out.println(new String(footerE) + "www.a.com.br " + new String(footerD));

    }

}
