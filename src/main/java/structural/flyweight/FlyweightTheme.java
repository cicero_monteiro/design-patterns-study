package structural.flyweight;

public interface FlyweightTheme {
    void print(String title, String text);
}
