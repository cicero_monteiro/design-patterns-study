package structural.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Presentation {
    
    private List<Slide> slides = new ArrayList();
    
    public void add(Slide slide) {
	this.slides.add(slide);
    }
    
    public void print() {
	this.slides.forEach(s -> s.print());
    }

}
