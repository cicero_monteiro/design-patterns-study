package structural.flyweight;

public class Slide {
    
    private FlyweightTheme theme;
    private String title;
    private String text;
    
    public Slide(FlyweightTheme theme, String title, String text) {
	super();
	this.theme = theme;
	this.title = title;
	this.text = text;
    }
    
    public void print() {
	this.theme.print(title, text);
    }

}
