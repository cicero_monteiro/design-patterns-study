package structural.flyweight;

import java.util.Arrays;

public class AsteriskTheme implements FlyweightTheme {

    @Override
    public void print(String title, String text) {
	System.out.println("----------------" + title + "-----------------");
	System.out.println(text);
	char[] footer = new char[22 + title.length()];
	Arrays.fill(footer, '-');
	System.out.println(footer);
    }

}
