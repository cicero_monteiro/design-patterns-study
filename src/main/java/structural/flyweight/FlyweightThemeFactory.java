package structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class FlyweightThemeFactory {
    
    private static Map<Class<? extends FlyweightTheme>, FlyweightTheme> themes = new HashMap();
    public static final Class<AsteriskTheme> ASTERISK = AsteriskTheme.class;
    public static final Class<HyphenTheme> HYPHEN = HyphenTheme.class;
    public static final Class<GenericTheme> GENERIC = GenericTheme.class;
    
    public static FlyweightTheme getTheme(Class<? extends FlyweightTheme> clazz) {
	if (!themes.containsKey(clazz)) {
	    try {
		themes.put(clazz, clazz.newInstance());
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	return themes.get(clazz);
    }
   

}
