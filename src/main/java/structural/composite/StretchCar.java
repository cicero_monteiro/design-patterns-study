package structural.composite;

public class StretchCar implements Stretch {
	
	private String direction;
	private double distance;

	public StretchCar(String direction, double distance) {
		super();
		this.direction = direction;
		this.distance = distance;
	}
	
	@Override
	public void print() {
		System.out.println("Go the car: ");
		System.out.println(this.direction);
		System.out.println("The distance traveled will be of: " + this.distance + " metros");
	}

}
