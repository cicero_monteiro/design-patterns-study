package structural.composite;

public class WalkingStretch implements Stretch {
	
	private String description;
	private double distance;

	public WalkingStretch(String description, double distance) {
		super();
		this.description = description;
		this.distance = distance;
	}

	@Override
	public void print() {
		System.out.println("Go Walking: " );
		System.out.println(this.description);
		System.out.println("The distance traveled will be of: " + this.distance + "metros");
	}

}
