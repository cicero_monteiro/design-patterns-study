package structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Way implements Stretch {
	
	private List<Stretch> stretches;
	
	public Way() {
		this.stretches = new ArrayList();
	}
	
	public void add(Stretch stretch) {
		this.stretches.add(stretch);
	}
	
	public void remove(String stretch) {
		this.stretches.remove(stretch);
	}

	@Override
	public void print() {
		this.stretches.forEach(Stretch::print);
	}

}
