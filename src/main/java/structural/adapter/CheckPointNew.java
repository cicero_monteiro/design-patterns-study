package structural.adapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import creator.objectpool.Employee;

public class CheckPointNew {
	
	public void register(Employee employee, boolean entry) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:m:s");
		String format = sdf.format(calendar.getTime());
		
		if (entry) {
			System.out.println("Entry: " + employee.getName() + " at " + format);

		} else {
			System.out.println("Exit: " + employee.getName() + " at " + format);
		}
		
	}

}
