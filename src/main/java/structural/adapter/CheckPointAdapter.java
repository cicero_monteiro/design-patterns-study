package structural.adapter;

import creator.objectpool.Employee;

public class CheckPointAdapter extends CheckPoint {
	
	private CheckPointNew checkPointNew;
	
	public CheckPointAdapter() {
		this.checkPointNew = new CheckPointNew();
	}
	
	@Override
	public void registerEntry(Employee employee) {
		this.checkPointNew.register(employee, true);
	}
	
	@Override
	public void registerExit(Employee employee) {
		this.checkPointNew.register(employee, false);
	}

}
