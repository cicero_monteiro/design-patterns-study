package structural.decorator;

import creator.factorymethod.Sender;

public class SenderDecoratorWithCriptograph extends SenderDecorator {

	public SenderDecoratorWithCriptograph(Sender sender) {
		super(sender);
	}

	@Override
	public void send(String message) {
		System.out.println("Sending criptograph message...");
		this.getSender().send(criptograph(message));
		
	}

	private String criptograph(String message) {
		String messageCriptograph = new StringBuilder(message).reverse().toString();
		return messageCriptograph;
	}

}
