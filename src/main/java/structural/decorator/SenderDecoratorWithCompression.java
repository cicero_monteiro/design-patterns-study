package structural.decorator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import creator.factorymethod.Sender;

public class SenderDecoratorWithCompression extends SenderDecorator {

	public SenderDecoratorWithCompression(Sender sender) {
		super(sender);
	}

	@Override
	public void send(String message) {
		System.out.println("Sending message compression: ");
		String messageCompression;
		
		try {
			messageCompression = compress(message);
		} catch (IOException e) {
			messageCompression = message;
		}
		this.getSender().send(messageCompression);
	}

	private String compress(String message) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DeflaterOutputStream dout = new DeflaterOutputStream(out, new Deflater());
		dout.write(message.getBytes());
		dout.close();
		return new String(out.toByteArray());
	}

}
