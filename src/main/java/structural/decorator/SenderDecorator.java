package structural.decorator;

import creator.factorymethod.Sender;

public abstract class SenderDecorator implements Sender {
	private Sender sender;

	public SenderDecorator(Sender sender) {
		super();
		this.sender = sender;
	}
	
	public abstract void send(String message);
	
	public Sender getSender() {
		return sender;
	}
	
}
