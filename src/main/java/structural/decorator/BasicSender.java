package structural.decorator;
import creator.factorymethod.Sender;

public class BasicSender implements Sender {

	@Override
	public void send(String message) {
		System.out.println("Sending Basic message..." + "\n");
	}

}
