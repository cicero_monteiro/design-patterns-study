import org.junit.Test;

import creator.factorymethod.Sender;
import structural.decorator.BasicSender;
import structural.decorator.SenderDecoratorWithCompression;
import structural.decorator.SenderDecoratorWithCriptograph;

public class SenderDecoratorTest {
	
	@Test
	public void shouldSendCriptograph() {
		String message = "";
		
		Sender senderCript = new SenderDecoratorWithCriptograph(new BasicSender());
		senderCript.send(message);
	}
	
	@Test
	public void shouldSendWithCompression() {
		String message = "";
		
		Sender senderCript = new SenderDecoratorWithCompression(new BasicSender());
		senderCript.send(message);
	}

}
