package creator.abstractfactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ComunicatorFactoryTest {

	private ComunicatorFactory comunicatorFactoryVisa;
	private ComunicatorFactory comunicatorFactoryMastercard;

	@Before
	public void setUp() {
		this.comunicatorFactoryVisa = new VisaComunicatorFactory();
		this.comunicatorFactoryMastercard = new MastercardComunicatorFactory();
	}

	@Test
	public void shouldVisaComunicatorFactory() {
		String transaction = "value=560;pass=123";
		Sender sender = this.comunicatorFactoryVisa.createSender();
		sender.send(transaction);

		Receiver receiver = this.comunicatorFactoryVisa.createReceiver();

		Assert.assertEquals("Visa message", receiver.receive());
	}
	
	@Test
	public void shouldMastercardComunicatorFactory() {
		String transaction = "value=560;pass=123";
		Sender sender = this.comunicatorFactoryMastercard.createSender();
		sender.send(transaction);

		Receiver receiver = this.comunicatorFactoryMastercard.createReceiver();

		Assert.assertEquals("Mastercard message", receiver.receive());
	}

}
