package creator.singleton;

import org.junit.Assert;
import org.junit.Test;


public class ConfigurationTest {
	
	@Test
	public void shouldSingletonConfiguration() {
		Configuration configuration = Configuration.getInstance();
		String timeZoneExpected = "America/Sao_Paulo";
		String currencyCodeExpected = "BRL";
		
		Assert.assertEquals(timeZoneExpected, configuration.getProperty("time-zone"));
		Assert.assertEquals(currencyCodeExpected, configuration.getProperty("currency-code"));
	}

}
