package creator.prototype;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class PrototypeTest {
	
	@Test
	public void shouldCampaignClone() {
		String name = "Cicero consulting";
		
		Calendar pay = Calendar.getInstance();
		pay.add(Calendar.DATE, 30);
		
		Set<String> hashSet = new HashSet();
		hashSet.add("Course");
		hashSet.add("Java");
		hashSet.add("Cicero consulting");
		
		Campaign campaign = new Campaign(name, pay, hashSet);
		
		Campaign clone = campaign.clone();
		
		Assert.assertEquals(campaign.getPay(), clone.getPay());
		
	}
	

}
