package creator.pool;

import org.junit.Assert;
import org.junit.Test;

import creator.objectpool.Employee;
import creator.objectpool.EmployeePool;
import creator.objectpool.Pool;

public class EmployeePoolTest {
	
	@Test
	public void shouldUtilityPool() {
		Pool<Employee> employeePool = new EmployeePool();
		Employee employee = employeePool.acquire();
		
		Assert.assertNotNull(employee);
	}

}
