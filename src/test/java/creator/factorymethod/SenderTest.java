package creator.factorymethod;

import org.junit.Before;
import org.junit.Test;

public class SenderTest {
	
	private SenderCreator creator;
	
	@Before
	public void setUp() {
		this.creator = new SenderCreator();
	}

	@Test
	public void testSendSMS() {
		Sender sender1 = creator.create(SenderType.SMS);
		sender1.send("Java is cool!");
	}

	@Test
	public void testSendEmail() {
		Sender sender2 = creator.create(SenderType.EMAIL);
		sender2.send("Java is cool!");
	}

	@Test
	public void testSendJMS() {
		Sender sender3 = creator.create(SenderType.JMS);
		sender3.send("Java is cool!");
	}

}
