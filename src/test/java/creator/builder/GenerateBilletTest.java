package creator.builder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GenerateBilletTest {

	private BilletBuilder billetBuilder;
	private BilletGenerator billetGenerator;

	@Before
	public void setUp() {
		this.billetBuilder = new BBBilletBuilder();
		this.billetGenerator = new BilletGenerator(billetBuilder);
	}

	@Test
	public void shouldGenerateBBBillet() {
		StringBuilder expectedBillet = new StringBuilder();

		expectedBillet.append("Billet BB").append("\n").append("Payer: Cicero Monteiro").append("\n").append("Assignor: Percy Oliveira").append("\n")
				.append("Value: 100.54").append("\n").append("Pay: 09/07/2016").append("\n").append("OurNumber: 1234");

		Billet billet = this.billetGenerator.generateBillet();
		
		Assert.assertEquals(expectedBillet.toString(), billet.toString());
		
	}

}
