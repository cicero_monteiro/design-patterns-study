package structural.flyweight;

import org.junit.Test;

import junit.framework.Assert;

public class ThemeTest {
    
    @Test
    public void shouldPrintPresentations() {
	Presentation presentation = new Presentation();
	presentation.add(new Slide(FlyweightThemeFactory.getTheme(GenericTheme.class), "Generic Theme", "Generic TExt"));
	presentation.add(new Slide(FlyweightThemeFactory.getTheme(AsteriskTheme.class), "Asterisk Theme", "Asterisk Theme TExt"));
	presentation.add(new Slide(FlyweightThemeFactory.getTheme(HyphenTheme.class), "Hyphen Theme", "Hyphen TExt"));
	
	presentation.print();
	
	Assert.assertNotNull(presentation);
    }

}
