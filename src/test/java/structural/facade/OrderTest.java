package structural.facade;

import org.junit.Before;
import org.junit.Test;

public class OrderTest {
	
	private Stock stock;
	private Financial financial;
	private PostSale postSale;
	private OrderFacade orderFacade;
	
	@Before
	public void setUp() {
		this.stock = new Stock();
		this.financial = new Financial();
		this.postSale = new PostSale();
		this.orderFacade = new OrderFacade(stock, financial, postSale);
	}
	
	@Test
	public void shouldScribeOrderFacade() {
		Order order = new Order("Notebook", "Josefina Alves", "Av Cafe 123 SP");
		this.orderFacade.scribeOrder(order);
	}

}
