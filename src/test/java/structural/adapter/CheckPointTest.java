package structural.adapter;

import org.junit.Test;

import creator.objectpool.Employee;

public class CheckPointTest {
	
	@Test
	public void shouldRegistrerEntryAndExit() throws InterruptedException {
		CheckPoint checkPoint = new CheckPointAdapter();
		Employee employee = new Employee("Josefina Alves");
		checkPoint.registerEntry(employee);
		Thread.sleep(3000);
		checkPoint.registerExit(employee);
	}

}
