package structural.composite;

import org.junit.Test;

public class StretchTest {
	
	@Test
	public void shouldCreateStretch() {
		Stretch stretch1 = new WalkingStretch("Vá até o cruzamento da Av . Rebouças com a Av . Brigadeiro Faria Lima",
				500);
		
		Stretch stretch2 = new WalkingStretch("Vá até o cruzamento da Av . Brigadeiro Faria Lima com a Av . Cidade Jardim",
				1500);

		
		Stretch stretch3 = new WalkingStretch("Vire a direita na Marginal Pinheiros",
				500);
		
		Way way1 = new Way();
		way1.add(stretch1);
		way1.add(stretch2);
		
		System.out.println("Stretch1: " );
		way1.print();
		
		Way way2 = new Way();
		way2.add(way1);
		way2.add(stretch3);
		System.out.println("--------------------" );
		System.out.println("Way 2: " );
		way2.print();
		
		
		

		
	}

}
