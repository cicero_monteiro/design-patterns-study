package structural.bridge;

import org.junit.Test;

public class ReceiptTest {
	
	@Test
	public void shouldGenerateFile() {
		GeneratorFile generatorFileTXT = new GeneratorFileTXT();
		Receipt receipt = new Receipt("Chagas Software", "Chagas", 100.0, generatorFileTXT);
		receipt.generateFile();
	}

}
